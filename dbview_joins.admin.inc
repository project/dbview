<?php

/**
 * @file
 * Administrative pages for Database View API - Joins section.
 */

/**
 * =================== Database view joins form @ admin/settings/dbview/N/joins ===================
 */

/**
 * Build a form to keep track of joins for each dbview.
 */
function dbview_dbivew_joins_form(&$form_state, $dbview = array()) {
  $dbview_joins = dbview_dbview_join_load_all($dbview['dbvid']);

  $form['dbview_joins']['#tree'] = TRUE;
  foreach ($dbview_joins as $dbvjid => $dbview_join) {
    $form['dbview_joins'][$dbvjid] = _dbview_dbivew_joins_join($dbview_join);
  }

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Back to Database views settings'),
  );

  // Set title dbview joins edit form.
  $dbview_name = $dbview['table_view_name'];
  $title = t('Joins for database view %dbview_name', array('%dbview_name' => $dbview_name));
  drupal_set_title($title);

  return $form;
}

/**
 * Builds the joins for a single dbview on the joins form.
 */
function _dbview_dbivew_joins_join($dbview_join) {
  // Add a value join to the form containing the join's unique ID.
  $form['dbvjid'] = array(
    '#type' => 'value',
    '#value' => $dbview_join['dbvjid'],
  );

  // Add a value join for the dbview's unique ID.
  $form['dbvid'] = array(
    '#type' => 'value',
    '#value' => $dbview_join['dbvid'],
  );

  // table join table.
  $form['table_join_table'] = array(
    '#type' => 'markup',
    '#value' => check_plain($dbview_join['table_join_table']),
  );

  // table join left field.
  $form['table_join_left_field'] = array(
    '#type' => 'markup',
    '#value' => check_plain($dbview_join['table_join_left_field']),
  );

  // table join field.
  $form['table_join_field'] = array(
    '#type' => 'markup',
    '#value' => check_plain($dbview_join['table_join_field']),
  );

  $form['operations'] = array(
    '#type' => 'markup',
    '#value' => _dbview_dbview_joins_links($dbview_join),
  );

  return $form;
}

/**
 * Build the operations links for a single dbview join.
 *
 * @param $dbview_join
 */
function _dbview_dbview_joins_links($dbview_join) {
  $path = drupal_get_path('module', 'dbview') .'/images/';

  // Operations links.
  $links['edit'] = array(
    'title' => theme('image', $path .'table_gear.png', t('Edit join'), t('Edit join "!id"', array('!id' => $dbview_join['dbvjid']))),
    'href' => 'admin/settings/dbview/'. $dbview_join['dbvid'] .'/joins/'. $dbview_join['dbvjid'],
    'html' => TRUE,
  );

  $links['delete'] = array(
    'title' => theme('image', $path .'table_delete.png', t('Delete join'), t('Delete join "!id"', array('!id' => $dbview_join['dbvjid']))),
    'href' => 'admin/settings/dbview/'. $dbview_join['dbvid'] .'/joins/'. $dbview_join['dbvjid'] .'/delete',
    'html' => TRUE,
  );

  return theme('links', $links);
}

/**
 * General submit handler for joins form.
 *
 * @ingroup formapi
 * @see dbview_dbivew_joins_form()
 */
function dbview_dbivew_joins_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/settings/dbview/'. $dbview_join['dbvid'];
}

/**
 * Theme joins form.
 *
 * Arranges dbviews joins in a table.
 */
function theme_dbview_dbivew_joins_form($form) {
  $rows = array();
  foreach (element_children($form['dbview_joins']) as $key) {
    $row = array();

    // Render the hidden 'join id' and the 'table_join_table' into the
    // same column of the row.
    $row[] = drupal_render($form['dbview_joins'][$key]['dbvjid']) . drupal_render($form['dbview_joins'][$key]['table_join_table']);

    // table join left field.
    $row[] = drupal_render($form['dbview_joins'][$key]['table_join_left_field']);

    // table join field.
    $row[] = drupal_render($form['dbview_joins'][$key]['table_join_field']);

    // Render the edit and delete links into their own column.
    $row[] = drupal_render($form['dbview_joins'][$key]['operations']);

    // Add the new row to our collection of rows.
    $rows[] = array(
      'data' => $row,
    );
  }

  // If there were no joins found, let users know.
  if (count($rows) == 0) {
    $rows[] = array(array('data' => t('No joins have been added.'), 'colspan' => 4));

  }

  // Render a list of header titles, and our array of rows, into a table. Even
  // we've already rendered all of our dbviews, we always call drupal_render()
  // on the form itself after we're done, so hidden security joins and other
  // elements (like buttons) will appear properly at the bottom of the form.
  $header = array(t('join table'), t('join left field'), t('join field'), t('Join operations: Edit | Delete'));
  // Add an optional caption that appears above the table.
  $caption = t('');
  $output = theme('table', $header, $rows, array('id' => 'dbviews-overview'), $caption);
  $output .= drupal_render($form);

  return $output;
}

/**
 * =================== Database view joins edit form @ admin/settings/dbview/N/joins/NN ===================
 */

/**
 * Build a joins edit form for each dbview join.
 *
 * @param $dbview
 *   An array of information about the parent dbview.
 *
 * @param $dbview_join
 *   An array of information about a single join associated with a dbview.
 */
function dbview_join_form(&$form_state, $dbview = array(), $dbview_join = array()) {
  // Set the default values for a new item. By using += rather than =, we
  // only overwrite array keys that have not yet been set. It's safe to use
  // on both an empty array, and an incoming array with full or partial data.
  $dbview_join += array(
    // Do not set 'dbvjid', so new joins are inserted properly.
    // @see dbview_dbview_join_save()
    // Pull 'dbvid' from $dbview param.
    'dbvid' => $dbview['dbvid'],
    'table_view_name' => $dbview['table_view_name'],
    'table_join_table' => '',
    'table_join_left_field' => '',
    'table_join_field' => '',
  );

  // If we're editing an existing join, we'll add a value field to the form
  // containing the join's unique ID.
  if (!empty($dbview_join['dbvjid'])) {
    $form['dbvjid'] = array(
      '#type' => 'value',
      '#value' => $dbview_join['dbvjid'],
    );
  }

  // Whether existing or new join, add a value for the dbview's unique ID.
  $form['dbvid'] = array(
    '#type' => 'value',
    '#value' => $dbview_join['dbvid'],
  );

  // Also add a value for the dbview's name.
  $form['table_view_name'] = array(
    '#type' => 'value',
    '#value' => $dbview_join['table_view_name'],
  );

  // Get ksorted list of all drupal tables.
  $tables = drupal_get_schema();
  foreach ($tables as $key => $table) {
    $table_options[$key] = $key;
  }
  ksort($table_options);

  // @TODO: consider adding autocomplete.
  $form['table_join_table'] = array(
    #'#type' => 'textfield',
    '#type' => 'select',
    '#title' => t('table_join_table'),
    '#required' => TRUE,
    '#default_value' => $dbview_join['table_join_table'],
    '#options' => $table_options,
    //'#autocomplete_path' => 'dbview/autocomplete',
  );

  $form['table_join_left_field'] = array(
    '#type' => 'textfield',
    '#title' => t('table_join_left_field'),
    '#required' => TRUE,
    '#default_value' => $dbview_join['table_join_left_field'],
  );

  // Get list of all fields available to the current database view.
  $dbview_fields = dbview_dbview_field_load_all($dbview_join['dbvid']);
  foreach ($dbview_fields as $dbview_field) {
    $field_options[$dbview_field['field_name']] = $dbview_field['field_name'];
  }

  $form['table_join_field'] = array(
    '#type' => 'select',
    '#title' => t('table_join_field'),
    '#required' => TRUE,
    '#default_value' => $dbview_join['table_join_field'],
    '#options' => $field_options,
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  // Only show the delete button if we already have an ID. Set the delete
  // button's submit handler to a custom function that should only fire if
  // this button is clicked. In all other cases, the form will fall back to
  // the default $form_id_submit() function.
  if (!empty($dbview_join['dbvjid'])) {
    $form['buttons']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('dbview_join_form_delete'),
    );
  }

  // Set title on dbview join edit form, depending on whether we're adding or editing a join.
  $dbview_name = $dbview['table_view_name'];
  $join_name = $dbview_join['table_join_table'] .'/'. $dbview_join['table_join_left_field'] .'/'. $dbview_join['table_join_field'];
  $add_txt = t('Add new join for database view %dbview_name', array('%dbview_name' => $dbview_name));
  $edit_txt = t('Edit join %join_name for database view %dbview_name', array('%join_name' => $join_name, '%dbview_name' => $dbview_name));
  $title = !empty($dbview_join['dbvjid']) ? $edit_txt : $add_txt;
  drupal_set_title($title);

  return $form;
}

/**
 * General submit handler for joins edit form.
 *
 * @see dbview_join_form()
 */
function dbview_join_form_submit($form, &$form_state) {
  $dbview_join = $form_state['values'];
  // Save values to database.
  dbview_dbview_join_save($dbview_join);
  $form_state['redirect'] = 'admin/settings/dbview/'. $dbview_join['dbvid'] .'/joins';
}

/**
 * General validation handler for joins edit form.
 *
 * @see dbview_join_form()
 */
function dbview_join_form_validate($form, $form_state) {
  $dbview_join = $form_state['values'];
  $table = $dbview_join['table_join_table'];
  $column = $dbview_join['table_join_left_field'];
  if (!db_table_exists($table)) {
    form_set_error('table_join_table', t('Table %table does not exist', array('%table' => $table)));
  }
  elseif (!db_column_exists($table, $column)) {
    form_set_error('table_join_left_field', t('Field %column is not an available field in table %table', array('%table' => $table, '%column' => $column)));
  }
}


/**
 * =================== Database view joins delete confirm form @ admin/settings/dbview/N/joins/NN/delete ===================
 */

/**
 * Delete button submit handler for module's add/edit form.
 *
 * Redirects to the 'delete join' confirmation page without performing any
 * operations.
 *
 * @ingroup formapi
 * @see dbview_join_form()
 */
function dbview_join_form_delete($form, &$form_state) {
  $dbview_join = $form_state['values'];
  $form_state['redirect'] = 'admin/settings/dbview/'. $dbview_join['dbvid'] .'/joins/'. $dbview_join['dbvjid'] .'/delete';
}

/**
 * Build the delete confirmation form.
 *
 * A simple wrapper around Drupal's core confirm_form() function. Adds a value
 * field to store the ID of the join being deleted.
 *
 * @ingroup forms
 * @see dbview_join_delete_confirm_submit()
 * @see confirm_form()
 */
function dbview_join_delete_confirm(&$form_state, $dbview_join) {
  $dbview = dbview_dbview_load($dbview_join['dbvid']);

  $form['dbvjid'] = array(
    '#type' => 'value',
    '#value' => $dbview_join['dbvjid'],
  );

  $form['dbvid'] = array(
    '#type' => 'value',
    '#value' => $dbview_join['dbvid'],
  );

  $join_title = $dbview_join['table_join_table'] .'/'. $dbview_join['table_join_left_field'] .'/'. $dbview_join['table_join_field'];

  return confirm_form($form,
    t('Are you sure you want to delete join "!join_title" for database view "!table_view_name"?', array('!join_title' => $join_title, '!table_view_name' => $dbview['table_view_name'])),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/dbview/'. $dbview_join['dbvid'] .'/joins',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * General submit handler for the delete confirmation form.
 *
 * Core's confirm_form() function adds the 'confirm' value element we check
 * against to ensure the form was properly submitted. If it's there, delete
 * the join and redirect to the overview form.
 *
 * @ingroup formapi
 * @see dbview_join_form()
 */

function dbview_join_delete_confirm_submit($form, &$form_state) {
  $dbview_join = $form_state['values'];
  if ($form_state['values']['confirm']) {
    dbview_dbview_join_delete($dbview_join['dbvjid']);
    drupal_set_message(t('Your join was deleted.'));
  }
  $form_state['redirect'] = 'admin/settings/dbview/'. $dbview_join['dbvid'] .'/joins';
}
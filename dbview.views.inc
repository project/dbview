<?php

/**
 * @file
 * The Database Views API module Views declaration.
 *
 * @see dbview_write_views_inc_data()
 */

function dbview_views_data() {
  $data = array();

  // Dynamically generates required information for dbview.views.inc.
  dbview_write_views_inc_data();
}
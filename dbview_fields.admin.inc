<?php

/**
 * @file
 * Administrative pages for Database View API - Fields section.
 */

/**
 * =================== Database view fields form @ admin/settings/dbview/N/fields ===================
 */

/**
 * Build a form to keep track of fields for each dbview.
 */
function dbview_dbivew_fields_form(&$form_state, $dbview = array()) {
  $dbview_fields = dbview_dbview_field_load_all($dbview['dbvid']);

  $form['dbview_fields']['#tree'] = TRUE;
  foreach ($dbview_fields as $dbvfid => $dbview_field) {
    $form['dbview_fields'][$dbvfid] = _dbview_dbivew_fields_field($dbview_field);
  }

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Back to Database views settings'),
  );

  // Set title dbview fields edit form.
  $dbview_name = $dbview['table_view_name'];
  $title = t('Fields for database view %dbview_name', array('%dbview_name' => $dbview_name));
  drupal_set_title($title);

  return $form;
}

/**
 * Builds the fields for a single dbview on the overview form.
 */
function _dbview_dbivew_fields_field($dbview_field) {
  // Add a value field to the form containing the field's unique ID.
  $form['dbvfid'] = array(
    '#type' => 'value',
    '#value' => $dbview_field['dbvfid'],
  );

  // Add a value field for the dbview's unique ID.
  $form['dbvid'] = array(
    '#type' => 'value',
    '#value' => $dbview_field['dbvid'],
  );

  // Field name.
  $form['field_name'] = array(
    '#type' => 'markup',
    '#value' => check_plain($dbview_field['field_name']),
  );

  // Field title.
  $form['field_title'] = array(
    '#type' => 'markup',
    '#value' => check_plain($dbview_field['field_title']),
  );

  // Field help.
  $form['field_help'] = array(
    '#type' => 'markup',
    '#value' => check_plain($dbview_field['field_help']),
  );

  // Real field alias.
  $form['real_field_alias'] = array(
    '#type' => 'markup',
    '#value' => check_plain($dbview_field['real_field_alias']),
  );

  // Operations.
  $form['operations'] = array(
    '#type' => 'markup',
    '#value' => _dbview_dbview_fields_links($dbview_field),
  );

  return $form;
}

/**
 * Build the operations links for a single dbview field.
 */
function _dbview_dbview_fields_links($dbview_field) {
  $path = drupal_get_path('module', 'dbview') .'/images/';

  $links['edit'] = array(
    'title' => theme('image', $path .'table_row_insert.png', t('Edit field'), t('Edit field "!field_title"', array('!field_title' => $dbview_field['field_title']))),
    'href' => 'admin/settings/dbview/'. $dbview_field['dbvid'] .'/fields/'. $dbview_field['dbvfid'],
    'html' => TRUE,
  );

  return theme('links', $links);
}

/**
 * General submit handler for fields form.
 *
 * @ingroup formapi
 * @see dbview_dbivew_fields_form()
 */
function dbview_dbivew_fields_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/settings/dbview/'. $dbview_field['dbvid'];
}

/**
 * Theme fields form.
 *
 * Arranges dbviews fields in a table.
 */
function theme_dbview_dbivew_fields_form($form) {
  $rows = array();
  foreach (element_children($form['dbview_fields']) as $key) {
    $row = array();

    // Render the hidden 'dbview id' field and the field_name of the field into the
    // same column of the row.
    $row[] = drupal_render($form['dbview_fields'][$key]['dbvfid']) . drupal_render($form['dbview_fields'][$key]['field_name']);

    // Field title.
    $row[] = drupal_render($form['dbview_fields'][$key]['field_title']);

    // Field help.
    $row[] = drupal_render($form['dbview_fields'][$key]['field_help']);

    // Real field alias.
    $row[] = drupal_render($form['dbview_fields'][$key]['real_field_alias']);

    // Render the edit and delete links into their own column.
    $row[] = drupal_render($form['dbview_fields'][$key]['operations']);

    // Add the new row to our collection of rows.
    $rows[] = array(
      'data' => $row,
    );
  }

  // If there were no fields found, let users know.
  if (count($rows) == 0) {
    $rows[] = array(array('data' => t('No fields have been added.'), 'colspan' => 5));
  }

  // Render a list of header titles, and our array of rows, into a table. Even
  // we've already rendered all of our dbviews, we always call drupal_render()
  // on the form itself after we're done, so hidden security fields and other
  // elements (like buttons) will appear properly at the bottom of the form.
  $header = array(t('Field name'), t('Field title'), t('Field help'), t('Real field alias'), t('Operations: edit field'));
  // Add an optional caption that appears above the table.
  $caption = t('');
  $output = theme('table', $header, $rows, array('id' => 'dbviews-overview'), $caption);
  $output .= drupal_render($form);

  return $output;
}

/**
 * =================== Database view fields edit form @ admin/settings/dbview/N/fields/NN ===================
 */

/**
 * Build a fields edit form for each dbview field.
 *
 * @param $dbview_field
 *   An array of information about a single field associated with a dbview.
 */
function dbview_field_form(&$form_state, $dbview_field = array()) {
  // Add a value field to the form containing the field's unique ID.
  $form['dbvfid'] = array(
    '#type' => 'value',
    '#value' => $dbview_field['dbvfid'],
  );

  // Add a value field for the dbview's unique ID.
  $form['dbvid'] = array(
    '#type' => 'value',
    '#value' => $dbview_field['dbvid'],
  );

  // Add a value field for the field's name.
  // We use this to set the form's page title whether or not we theme as a table
  $form['field_name'] = array(
    '#type' => 'value',
    '#value' => $dbview_field['field_name'],
  );

  $form['field_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Field title'),
    '#required' => TRUE,
    '#default_value' => $dbview_field['field_title'],
  );

  $form['field_help'] = array(
    '#type' => 'textfield',
    '#title' => t('Field help'),
    '#required' => TRUE,
    '#default_value' => $dbview_field['field_help'],
  );

  $form['real_field_alias'] = array(
    '#type' => 'textfield',
    '#title' => t('Real field alias'),
    '#required' => TRUE,
    '#default_value' => $dbview_field['real_field_alias'],
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  // Set title on dbview field edit form.
  $field_name = $dbview_field['field_name'];
  $dbview = dbview_dbview_load($dbview_field['dbvid']);
  $dbview_name = $dbview['table_view_name'];
  $title = t('Edit field %field_name for database view %dbview_name', array('%field_name' => $field_name, '%dbview_name' => $dbview_name));
  drupal_set_title($title);

  return $form;
}

/**
 * General submit handler for fields edit form.
 *
 * @see dbview_field_form()
 */
function dbview_field_form_submit($form, &$form_state) {
  $dbview_field = $form_state['values'];
  // Save values to database.
  dbview_dbview_field_save($dbview_field);
  $form_state['redirect'] = 'admin/settings/dbview/'. $dbview_field['dbvid'] .'/fields';
}
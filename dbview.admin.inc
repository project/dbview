<?php

/**
 * @file
 * Administrative pages for Database View API.
 */

/**
 * =================== Database views overview form @ admin/settings/dbview ===================
 */

/**
 * Build an overview form to keep track of dbviews.
 */
function dbview_overview_form(&$form_state) {
  $dbviews = dbview_dbview_load_all();

  $form['dbviews']['#tree'] = TRUE;
  foreach ($dbviews as $dbvid => $dbview) {
    $form['dbviews'][$dbvid] = _dbview_overview_dbview_field($dbview);
  }

  return $form;
}

/**
 * Builds the fields for a single dbview on the overview form.
 */
function _dbview_overview_dbview_field($dbview) {
  $form['dbvid'] = array(
    '#type' => 'hidden',
    '#value' => $dbview['dbvid'],
  );

  $form['table_view_name'] = array(
    '#type' => 'markup',
    '#value' => check_plain($dbview['table_view_name']),
  );

  $form['table_base_field'] = array(
    '#type' => 'markup',
    '#value' => check_plain($dbview['table_base_field']),
  );

  $form['table_base_title'] = array(
    '#type' => 'markup',
    '#value' => check_plain($dbview['table_base_title']),
  );

  $form['table_base_help'] = array(
    '#type' => 'markup',
    '#value' => check_plain($dbview['table_base_help']),
  );

  $form['table_base_database'] = array(
    '#type' => 'markup',
    '#value' => check_plain($dbview['table_base_database']),
  );

  $form['fields'] = array(
    '#type' => 'markup',
    '#value' => _dbview_dbview_links($dbview, 'fields'),
  );

  $form['joins'] = array(
    '#type' => 'markup',
    '#value' => _dbview_dbview_links($dbview, 'joins'),
  );

  $form['operations'] = array(
    '#type' => 'markup',
    '#value' => _dbview_dbview_links($dbview, 'edit'),
  );

  return $form;
}

/**
 * Build the operations links for a single dbview.
 *
 * @param $dbview
 *
 * @param $op
 *   can be:
 *   'edit'
 *   'fields'
 *   'joins'
 */

function _dbview_dbview_links($dbview, $op = 'edit') {
  $path = drupal_get_path('module', 'dbview') .'/images/';

  switch ($op) {
    case 'edit':
    $links['edit'] = array(
      'title' => theme('image', $path .'table_edit.png', t('Edit dbview'), t('Edit dbview')),
      'href' => 'admin/settings/dbview/'. $dbview['dbvid'] .'/edit',
      'html' => TRUE,
    );
    break;

    case 'fields':
    $fields_count = $dbview['fields'] > 0 ? $dbview['fields'] : t('None');
    $links['fields_count'] = array('title' => $fields_count);
    $links['fields'] = array(
      'title' => theme('image', $path .'table_row_insert.png', t('Edit fields'), t('Edit fields')),
      'href' => 'admin/settings/dbview/'. $dbview['dbvid'] .'/fields',
      'html' => TRUE,
    );
    break;

    case 'joins':
    $joins_count = $dbview['joins'] > 0 ? $dbview['joins'] : t('None');
    $links['joins_count'] = array('title' => $joins_count);
    $links['joins'] = array(
      'title' => theme('image', $path .'table_relationship.png', t('Edit joins'), t('Edit joins')),
      'href' => 'admin/settings/dbview/'. $dbview['dbvid'] .'/joins',
      'html' => TRUE,
    );
    break;
  }

  return theme('links', $links);
}

/**
 * Theme overview form.
 *
 * Arranges dbviews in a table.
 *
 * @ingroup themeable
 * @ingroup forms
 * @see dbview_overview_form()
 */
function theme_dbview_overview_form($form) {
  $rows = array();
  foreach (element_children($form['dbviews']) as $key) {
    $row = array();

    // Render the hidden 'dbview id' field and the title of the dbview into the
    // same column of the row.
    $row[] = drupal_render($form['dbviews'][$key]['dbvid']) . drupal_render($form['dbviews'][$key]['table_view_name']);

    // Base field.
    $row[] = drupal_render($form['dbviews'][$key]['table_base_field']);

    // Base title.
    $row[] = drupal_render($form['dbviews'][$key]['table_base_title']);

    // Base help.
    $row[] = drupal_render($form['dbviews'][$key]['table_base_help']);

    // Base database.
    $row[] = drupal_render($form['dbviews'][$key]['table_base_database']);

    // Fields.
    $row[] = drupal_render($form['dbviews'][$key]['fields']);

    // Joins.
    $row[] = drupal_render($form['dbviews'][$key]['joins']);

    // Render the edit and delete links into their own column.
    $row[] = drupal_render($form['dbviews'][$key]['operations']);

    // Add the new row to our collection of rows, and give it the 'draggable'
    // class, indicating that it should be... well, draggable.
    $rows[] = array(
      'data' => $row,
    );
  }

  // If there were no dbviews found, let users know.
  if (count($rows) == 0) {
    $rows[] = array(array('data' => t('No database views have been added.'), 'colspan' => 8));
  }

  // Render a list of header titles, and our array of rows, into a table. Even
  // we've already rendered all of our dbviews, we always call drupal_render()
  // on the form itself after we're done, so hidden security fields and other
  // elements (like buttons) will appear properly at the bottom of the form.
  $header = array(t('Dbview name'), t('Base field'), t('Base title'), t('Base help'), t('Base database'), t('Fields | edit'), t('Joins | edit'), t('Edit dbview'));
  // Add an optional caption that appears above the table.
  $caption = t('');
  $output = theme('table', $header, $rows, array('id' => 'dbviews-overview'), $caption);
  $output .= drupal_render($form);

  return $output;
}

/**
 * =================== Database views edit form @ admin/settings/dbview/N/edit ===================
 */

/**
 * Build the dbview editing form.
 *
 * If a dbview is passed in, an edit form with both Save and Delete buttons will
 * be built. Otherwise, a blank 'add new dbview' form, without the Delete button,
 * will be built.
 *
 * @ingroup forms
 * @see dbview_form_submit()
 * @see dbview_form_delete()
 */
function dbview_form(&$form_state, $dbview = array()) {
  // Set the default values for a new item. By using += rather than =, we
  // only overwrite array keys that have not yet been set. It's safe to use
  // on both an empty array, and an incoming array with full or partial data.
  $dbview += array(
    'table_view_name' => '',
    'table_base_field' => '',
    'table_base_title' => '',
    'table_base_help' => '',
    'table_base_database' => '',
    'fields' => '',
    'joins' => '',
  );

  // If we're editing an existing dbview, we'll add a value field to the form
  // containing the dbview's unique ID.
  if (!empty($dbview['dbvid'])) {
    $form['dbvid'] = array(
      '#type' => 'value',
      '#value' => $dbview['dbvid'],
    );
  }

  $form['table_view_name'] = array(
    '#type' => 'value',
    '#value' => $dbview['table_view_name'],
  );

  $form['table_base_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Base field'),
    '#required' => TRUE,
    '#default_value' => $dbview['table_base_field'],
  );

  $form['table_base_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Base title'),
    '#required' => TRUE,
    '#default_value' => $dbview['table_base_title'],
  );

  $form['table_base_help'] = array(
    '#type' => 'textfield',
    '#title' => t('Base help'),
    '#required' => TRUE,
    '#default_value' => $dbview['table_base_help'],
  );

  $form['table_base_database'] = array(
    '#type' => 'textfield',
    '#title' => t('Base database'),
    '#required' => TRUE,
    '#default_value' => $dbview['table_base_database'],
  );

  // Add a value field to the form containing the number of fields.
  $form['fields'] = array(
    '#type' => 'value',
    '#value' => $dbview['fields'],
  );

  // Add a value field for the number of joins.
  $form['joins'] = array(
    '#type' => 'value',
    '#value' => $dbview['joins'],
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  // Set title on dbview edit form.
  $dbview_name = $dbview['table_view_name'];
  $title = t('Edit database view %dbview_name', array('%dbview_name' => $dbview_name));
  drupal_set_title($title);

  return $form;
}

/**
 * General submit handler for the dbviews edit form.
 *
 * Simply passes incoming form values on to the module's CRUD save function,
 * then redirects to the overview form.
 *
 * @ingroup formapi
 * @see dbview_form()
 */
function dbview_form_submit($form, &$form_state) {
  $dbview = $form_state['values'];
  dbview_save($dbview);
  $form_state['redirect'] = 'admin/settings/dbview';
}